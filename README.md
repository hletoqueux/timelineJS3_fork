# TimelineJS

## Historique

Cet outil de création de timeline est basé sur l'excellent projet TimelineJS 3 de Knight Lab.

http://timeline.knightlab.com/

Ce projet est hébergé sur Github à l'adresse suivante :

https://github.com/NUKnightLab/TimelineJS3

Des exemples de jolies Timelines sont visibles à cette adresse :

http://timeline.knightlab.com/#examples

A l'origine, l'outil TimelineJS est basé sur une feuille de calcul GoogleSpreadsheet, qu'il suffit de remplir pour obtenir le rendu espéré.
Cette solution est utilisable pour des données opensource mais plus compliquée à employer lorsque les données à intégrer sont confidentielles, ou que l'on souhaite simplement visualiser sa timeline sur un support non connecté à internet (clef usb, présentation, etc...)..

Par ailleurs, la version standard de TimelineJS ne permet ni de lire des vidéos autres que celles de Vimeo ou Youtube, ni d'afficher les formats en les agrandissant.

Deux scripts Javascript ont donc été ajoutés à TimelineJS :

- AfterglowJS, une bibliothèque de lecture vidéo légère et élégante : https://afterglowplayer.com/
- FeatherlightJS, une bibliothèque basée sur JQuery permettant d'afficher des images plein écran : https://noelboss.github.io/featherlight/

Un répertoire appelé "Multimedia" a enfin été ajouté, pour héberger "en dur" les fichiers multimédia nécessaires.


## Utilisation

Les données temporelles et textuelles sont écrite dans un fichier *json*, qu'il est nécessaire de remplir à la main. Un exemple intitulé *nice.json* se trouve dans le répertoire *example*.

Le fichier débute par un titre (title) :
```
"title":{
		"text":{
			"headline":"Demo de Timeline",
			"text":"Ceci est une demo de Timeline.<p>Pour sauter une ligne, il suffit de mettre le texte entre deux balises html p et /p, comme en html. </p>"
			},
		"media": {
                "caption": "./Multimedia/serviette.jpg",
                "credit": "",
                "url": "./Multimedia/serviette.jpg"
            },
		"background": {
				"url": "./Multimedia/plage.jpg",
                "color": "#999999",
                "opacity": 75                
            }
	},
```
Il se poursuit ensuite avec des évènements (event), avec une date et heure de début, une date de fin optionnelle, un background qui est l'image de fond de la slide, un media qui peut être une vidéo mp4, ou un fichier image png ou jpg.
Enfin, un texte avec un titre et un contenu.
```
"events": [
        
		{
            "start_date": {
                "year":			"2016",
                "month":		"07",
                "day": 			"14",
                "hour": 		"21",
                "minute": 		"34",
                "second": 		"47",
                "millisecond": 	"",
                "format": 		"",
				"display_date":	"14 juillet 2016 à 21h34"
            },
            "end_date": {
                "year":			"2016",
                "month":		"07",
                "day": 			"14",
                "hour": 		"21",
                "minute": 		"38",
                "second": 		"00",
                "millisecond": 	"",
                "format": 		"",
				"display_date":	" "
            },        
			
            "background": {
				
                "color": "#999999",
                "opacity": 75,
                "url": "./Multimedia/plage.jpg"
            },
            "media": {
                "caption": "<video class='afterglow' id='1' width='720' height='500'><source type='video/mp4' src='./Multimedia/Tourists.mp4' />",
                "credit": "",
                "url": "./Multimedia/Tourists.mp4",
                "thumb": 	""
            },
            "text": {
                "headline": "21h34",
                "text": "A 21h34, les touristes sortent. <p>A 21H35, ils sont dehors.</p> On peut cliquer droit sur l'image de la video pour lancer la video."
            },
            "type": "overview"
        }
```
Pour ajouter des évènements, il suffit de copier le texte du précédent à partir de  :
```
{
            "start_date": {
                "year":			"2016",
                "month":		"07",
```
Et le coller après la virgule du précédent.

Les slides n'ont pas à être dans un ordre particulier : c'est la date de début et de fin qui conditionne son positionnement sur la timeline.

__**Le format json est très sensible au balisage. Au moindre faux pas, la timeline ne s'affichera pas.**__

A titre d'exemple, il est préférable d'utiliser le guillemet simple dans un texte ou dans les balises Javascript; L'usage d'un guillement double est mal interprèté par le json.

## Usage de Afterglow et Featherlight

- Pour Afterglow (fichiers vidéo)
Les vidéos doivent être encodées et enregistrées au format mp4
```
"media": {
                "caption": "<video class='afterglow' id='1' width='720' height='500'><source type='video/mp4' src='./Multimedia/Tourists.mp4' />",
                "credit": "",
                "url": "./Multimedia/Tourists.mp4",
                "thumb": 	""
            }
```

Le caption portera le balisage video avec la référence à afterglow. Notez les simple quote (guillement simple) pour les paramètres.
L'url pointera vers le répertoire Multimédia.

- Pour Featherlight
```
"media": {
                "caption": "<a href='./Multimedia/serviette.jpg' data-featherlight='image'>Agrandir l'image</a>",
                "credit": "",
                "url": "./Multimedia/serviette.jpg",
                "thumb": 	""
            }
```
Le fonctionnement est le même.


Il est possible de se référer aux documentation de [Featherlight](https://github.com/noelboss/featherlight/#installation) et d'[Afterglow](http://docs.afterglowplayer.com/docs/player-parameters) pour des paramètres supplémentaires.

## Modification du fichier timeline.html :

Une fois ajouté les premières lignes d'un nouveau fichier json, il faut paramétrer le fichier timeline.html qui se trouve à la racine du projet.
```
<html lang="fr">
  <head>
    <title>Nice - Un exemple de Timeline</title>
    <meta charset="utf-8">
    <meta name="description" content="Nice - 14 juillet 2016">
```

Modifier ici le titre de votre projet dans *title* et *content*.

```
<script>
      var timeline = new TL.Timeline('timeline', 'examples/Nice.json', {
      	theme_color: "#288EC3",
      	ga_property_id: ""
      });
    </script>
```
Enfin, en bas du fichier html, penser à faire pointer le fichier html sur votre fichier json.



## ToDo list :

Il serait souhaitable de pouvoir concevoir un outil, standalone, simple, qui puisse transformer un fichier ods pré-formaté en un json compatible avec TimelineJS.
Mes compétences en code sont limitées... Un volontaire? ^^

## Nota Bene :

Les fichiers multimédia fournis à titre d'exemple sont issus de sources ouvertes et libres de droit.